import api from '../api/api';
import ApiRoutes from '../api/routes/routes.api';

const apiServices = {
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    getAnimes: async (page: number, limit: number) => {
        const url = ApiRoutes.get_animes(page, limit);
        const response = await api.get(url);

        return response.data;
    },

    getDetail: async (id: number | string) => {
        const url = ApiRoutes.get_detail(id);
        const response = await api.get(url);

        return response.data.data;
    },
};

export default apiServices;
