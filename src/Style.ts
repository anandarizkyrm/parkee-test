export const globalStyles = {
    body: {
        margin: 0,
        fontFamily:
            'Helvetica neue,Helvetica,lucida grande,tahoma,verdana,arial,sans-serif',
        padding: 0,
        overflowY: 'auto',
        scrollbarWidth: 'thin',
        scrollbarColor: '#888888 #f4f4f4',
    },
    '*::-webkit-scrollbar': {
        width: '8px',
    },
    '*::-webkit-scrollbar-track': {
        backgroundColor: '#f4f4f4',
    },
    '*::-webkit-scrollbar-thumb': {
        backgroundColor: '#888888',
    },

    p: {
        margin: 0,
        padding: 0,
    },

    h1: {
        margin: 0,
        padding: 0,
    },

    h2: {
        margin: 0,
        padding: 0,
    },
};
