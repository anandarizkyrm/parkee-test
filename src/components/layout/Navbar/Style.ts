import { css } from '@emotion/react';

const gray = '#d1d5db';
export const stye_navigation = css`
    width: 100%;
    margin-top: 0;
    top: 0;
    color: #ffff;
    box-sizing: border-box;
    left: 0;
    height: 2rem;
    display: flex;
    align-items: center;
    justify-content: end;
    padding: 0 0.5rem 0.2rem 0.5rem;
    background-color: rgba(0, 0, 0, 0.7);

    @media (max-width: 768px) {
        font-size: 0.75rem;
        width: 30px;
    }
`;

export const style_logo = css`
    font-weight: 500;
    font-size: 1rem;
    @media (max-width: 768px) {
        gap: 8px;
    }
`;

export const style_btn = css`
    display: flex;
    background: none;
    font-size: 0.875rem;

    align-items: center;
    justify-content: center;
    outline: none;
    border: 1px solid ${gray};
    padding: 0.75rem;
    cursor: pointer;
    border: none;
    height: 100%;
    @media (max-width: 768px) {
        padding: 0.5rem;
        font-size: 0.75rem;
    }
`;
