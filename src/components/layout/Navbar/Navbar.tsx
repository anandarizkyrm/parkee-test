/** @jsxImportSource @emotion/react */

import { useLocation, useNavigate } from 'react-router-dom';
import { style_header } from '../../../pages/Home/Style';

const Navbar = () => {
    const location = useLocation();
    const navigate = useNavigate();

    const handleNavigate = () => {
        navigate('/');
    };
    return (
        <header>
            {location.pathname === '/' ? (
                <h2 onClick={handleNavigate} css={style_header}>
                    HOME (家)
                </h2>
            ) : (
                <h2 onClick={handleNavigate} css={style_header}>
                    BACK TO HOME (家に帰る)
                </h2>
            )}
        </header>
    );
};

export default Navbar;
