import { css } from '@emotion/react';

export const style_container = css`
    height: 100%;
    width: 100%;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 10;
`;

// border-top-left-radius: 0.5rem;
// border-bottom-left-radius: 0.5rem;

// border-top-right-radius: 0.5rem;
// border-bottom-right-radius: 0.5rem;
// border: 1px solid ${gray};
