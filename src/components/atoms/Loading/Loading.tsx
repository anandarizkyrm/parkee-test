/** @jsxImportSource @emotion/react */
import React from 'react';
import { ClipLoader } from 'react-spinners';
import { style_container } from './Style';

type Props = {
    loading: boolean;
};

const Loading = ({ loading }: Props) => {
    return (
        <div
            style={{ display: `${loading ? 'flex' : 'none'}` }}
            css={style_container}
        >
            <ClipLoader
                color={'#ffff'}
                loading={loading}
                cssOverride={{
                    display: 'block',
                    margin: '0 auto',
                }}
                size={150}
                aria-label="Loading Spinner"
                data-testid="loader"
            />
        </div>
    );
};

export default Loading;
