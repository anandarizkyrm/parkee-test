/** @jsxImportSource @emotion/react */
import React, { useEffect } from 'react';
import { useState } from 'react';
import { stye_container, stye_page, style_btn } from './Style';

type Props = {
    page: number;
    setPage: React.Dispatch<React.SetStateAction<number>>;
    totalData: number;
    limitPage: number;
};

const Pagination = ({ page, setPage, totalData, limitPage }: Props) => {
    const max = 5;
    const [startFromTo, setStartFromTo] = useState({
        from: 1,
        to: max,
    });

    const getLastPage = Math.floor(totalData / limitPage);

    function handleButtonClick(type: 'prev' | 'next') {
        if (type === 'prev') {
            if (page === 1) {
                return;
            }

            return setPage(page - 1);
        }

        return setPage(page + 1);
    }

    useEffect(() => {
        localStorage.setItem('page', String(page));
        const newFrom = Math.floor((page - 1) / max) * max + 1;
        const newTo = newFrom + max - 1;

        if (page < startFromTo.from || page > startFromTo.to) {
            setStartFromTo({
                from: newFrom,
                to: newTo,
            });
        }
    }, [page, startFromTo]);

    return (
        <div css={stye_container}>
            <button
                disabled={page === 1}
                css={style_btn}
                onClick={() => handleButtonClick('prev')}
            >
                Previous
            </button>

            <div
                css={stye_page}
                onClick={() => setPage(1)}
                style={{
                    background: `${page === 1 ? '#d1d5db' : 'none'} `,
                }}
            >
                {1}
            </div>

            <div
                css={stye_page}
                style={{
                    display: `${max - 1 <= startFromTo.from ? 'flex' : 'none'}`,
                }}
            >
                ...
            </div>
            {Array.from({ length: max }, (_, i) =>
                i + startFromTo.from !== getLastPage &&
                i + startFromTo.from !== 1 &&
                i + startFromTo.from < getLastPage ? (
                    <div
                        css={stye_page}
                        onClick={() => setPage(i + startFromTo.from)}
                        style={{
                            background: `${
                                page === i + startFromTo.from
                                    ? '#d1d5db'
                                    : 'none'
                            } `,
                        }}
                        key={i}
                    >
                        {i + startFromTo.from}
                    </div>
                ) : null,
            )}
            <div
                css={stye_page}
                style={{
                    display: `${
                        getLastPage - startFromTo.from <= max ? 'none' : 'flex'
                    }`,
                }}
            >
                ...
            </div>
            <div
                css={stye_page}
                onClick={() => setPage(getLastPage)}
                style={{
                    background: `${page === getLastPage ? '#d1d5db' : 'none'} `,
                }}
            >
                {getLastPage}
            </div>

            <button
                disabled={getLastPage === page}
                css={style_btn}
                onClick={() => handleButtonClick('next')}
            >
                Next
            </button>
        </div>
    );
};

export default Pagination;
