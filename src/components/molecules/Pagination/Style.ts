import { css } from '@emotion/react';

const gray = '#d1d5db';
export const stye_page = css`
    font-size: 0.875rem;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
    align-items: center;
    cursor: pointer;
    width: 36px;
    height: 100%;

    @media (max-width: 768px) {
        font-size: 0.75rem;
        width: 30px;
    }
`;
// border-top: 1px solid ${gray};
// border-bottom: 1px solid ${gray};
// border-right: 1px solid ${gray};

export const stye_container = css`
    display: flex;
    justify-content: center;
    height: 2rem;
    margin: 1rem 0 1rem 0;
    width: 100%;
    gap: 12px;
    @media (max-width: 768px) {
        gap: 8px;
    }
`;

export const style_btn = css`
    display: flex;
    background: none;
    font-size: 0.875rem;

    align-items: center;
    justify-content: center;
    outline: none;
    border: 1px solid ${gray};
    padding: 0.75rem;
    cursor: pointer;
    border: none;
    height: 100%;
    @media (max-width: 768px) {
        padding: 0.5rem;
        font-size: 0.75rem;
    }
`;
// border-top-left-radius: 0.5rem;
// border-bottom-left-radius: 0.5rem;

// border-top-right-radius: 0.5rem;
// border-bottom-right-radius: 0.5rem;
// border: 1px solid ${gray};
