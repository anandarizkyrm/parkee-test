/** @jsxImportSource @emotion/react */

import React from 'react';
import { StarIcon, UserIcon } from '@heroicons/react/24/solid';
import {
    style_card,
    style_content,
    style_icon,
    style_misc,
    style_people,
    style_rank,
    style_rating,
    style_title,
    style_type,
} from './Style';
import { useNavigate } from 'react-router-dom';

type AnimeProps = {
    backgroundUrl: string;
    id: string;
    slug: string;
    title: { en: string; en_jp: string; en_us: string; ja_jp: string };
    averageRating: string;
    userCount: number;
    popularityRank: number;
    showType: string;
    episodeCount: number;
};

const Card = ({
    backgroundUrl = 'https://media.kitsu.io/anime/poster_images/7442/large.jpg',
    id,
    slug,
    title,
    averageRating,
    userCount,
    popularityRank,
    showType,
    episodeCount,
}: AnimeProps) => {
    const navigate = useNavigate();
    return (
        <div
            css={style_card}
            onClick={() => navigate(`detail/${id}/${slug}`)}
            style={{
                backgroundImage: `url(${backgroundUrl})`,
            }}
        >
            <div css={style_rank}>{popularityRank}</div>
            <div css={style_content}>
                <h2 css={style_title}>
                    {title.en} ({title.ja_jp})
                </h2>
                <div css={style_misc}>
                    <span css={style_type}>
                        {showType}({episodeCount})
                    </span>
                    <span css={style_rating}>
                        <StarIcon css={style_icon} />
                        {averageRating}
                    </span>
                    <span css={style_people}>
                        <UserIcon css={style_icon} />
                        {userCount.toLocaleString()}
                    </span>
                </div>
            </div>
        </div>
    );
};

export default Card;
