import { css } from '@emotion/react';

export const style_card = css`
    height: 22rem;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    position: relative;
    cursor: pointer;

    &::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        transition: background-color 0.3s ease-in-out;
        background-color: transparent;
    }
    &:hover::after {
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 100;
    }m
`;
export const style_icon = css`
    height: 0.75rem;
    width: 0.75rem;
`;

export const style_rank = css`
    position: absolute;
    top: 0;
    font-size: 1.5rem;
    min-width: 32px;
    text-align: center;
    font-weight: bold;
    color: #ffff;
    background-color: rgba(0, 0, 0, 0.7);
    padding: 6px;
    left: 0;
`;
export const style_content = css`
    height: 12%;
    background-color: rgba(0, 0, 0, 0.7);
    bottom: 0;
    color: #fff;
    display: block;
    box-sizing: border-box;
    left: 0;
    font-size: 0.75rem;
    padding: 6px;
    width: 100%;
    position: absolute;
`;

export const style_title = css`
    font-size: 0.75rem;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    margin: 0;
`;

export const style_misc = css`
    display: flex;
    gap: 0.4em;
    margin-top: 0.1em;
`;

export const style_type = css`
    display: flex;
`;

export const style_rating = css`
    display: flex;
`;

export const style_people = css`
    display: flex;
`;

// border-top-left-radius: 0.5rem;
// border-bottom-left-radius: 0.5rem;

// border-top-right-radius: 0.5rem;
// border-bottom-right-radius: 0.5rem;
// border: 1px solid ${gray};
