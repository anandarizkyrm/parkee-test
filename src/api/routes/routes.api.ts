const ApiRoutes = {
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    get_animes(page: number = 1, per_page: number = 10) {
        return `/anime?sort=popularityRank&page[limit]=${per_page}&page[offset]=${page}`;
    },
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    get_detail(id: number | string) {
        return `/anime/${id}`;
    },
};

export default ApiRoutes;
