import axios, { AxiosError } from 'axios';

const errorHandler = (error: AxiosError) => {
    return Promise.reject(error);
};

const config = {
    headers: {
        'Content-Type': 'application/json',
    },
};

const api = axios.create({
    baseURL: 'https://kitsu.io/api/edge',
    headers: config.headers,
});

api.interceptors.response.use(undefined, (error) => errorHandler(error));

export default api;
