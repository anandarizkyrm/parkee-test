import { Route, Routes } from 'react-router-dom';
import Detail from './pages/Detail/Detail';
import Home from './pages/Home/Home';

function App() {
    return (
        <>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/detail/:id/:slug" element={<Detail />} />
            </Routes>
        </>
    );
}

export default App;
