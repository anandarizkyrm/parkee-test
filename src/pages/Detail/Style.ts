import { css } from '@emotion/react';

export const style_main_container = css`
    padding: 6px;
    margin-bottom: 32px;

    @media (max-width: 768px) {
        padding: 2px;
    }
`;
export const style_container = css`
    display: flex;
    gap: 32px;

    @media (max-width: 768px) {
        flex-direction: column;
    }
`;

export const styled_synopsis = css`
    font-size: 1rem;
    text-align: justify;
    @media (max-width: 768px) {
        font-size: 0.8rem;
    }
`;

export const style_popularity = css`
    font-size: 1.5rem;
    display: flex;
    align-items: center;
    color: gray;
    @media (max-width: 768px) {
        font-size: 1rem;
    }
`;

export const styled_score_title = css`
    background-color: rgba(0, 0, 0, 0.5);
    color: white;
    font-size: 0.85rem;
    padding: 12px;
    text-align: center;

    @media (max-width: 768px) {
        font-size: 0.7rem;
    }
`;

export const style_content = css`
    border-left: 1px solid #a5a5a5a5;
    padding: 0px 16px;

    @media (max-width: 768px) {
        padding: 2px;
    }
`;

export const style_title = css`
    @media (max-width: 768px) {
        font-size: 1.4rem;
    }
`;

export const styled_score = css`
    text-align: center;
    font-size: 4rem;
    color: gray;
    padding: 12px;

    @media (max-width: 768px) {
        font-size: 2rem;
        padding: 8px;
    }
`;

export const styled_user = css`
    font-size: 0.85rem;
    display: flex;
    gap: 2px;
    color: gray;
    align-items: center;
    justify-content: center;
    padding: 12px;

    @media (max-width: 768px) {
        font-size: 0.7rem;
        padding: 8px;
    }
`;

export const style_tag = css`
    display: flex;
    margin-top: 6px;
    gap: 12px;
`;

export const style_image_poster_container = css`
    width: 100%;
    height: auto;
    display: flex;
    justify-content: center;
`;

export const style_image_poster = css`
    max-height: 650px;
    height: auto;
    max-height: 540px;
    min-width: 100%;

    @media (max-width: 768px) {
        min-width: 40px;
    }
`;
export const style_content_rating_cover_image = css`
    display: flex;
    margin-top: 12px;
    gap: 22px;
`;

export const style_image_cover_container = css`
    width: 100%;
    position: relative;
`;

export const style_image_cover = css`
    width: 100%;
    height: 100%;
    object-fit: cover;
`;
