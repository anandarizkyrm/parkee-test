/** @jsxImportSource @emotion/react */
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import Loading from '../../components/atoms/Loading/Loading';
import apiServices from '../../services/api.service';
import { AnimeType } from '../../types/animes';
import {
    styled_score,
    styled_score_title,
    styled_synopsis,
    styled_user,
    style_container,
    style_content,
    style_content_rating_cover_image,
    style_image_cover,
    style_image_cover_container,
    style_image_poster,
    style_image_poster_container,
    style_main_container,
    style_popularity,
    style_tag,
    style_title,
} from './Style';

function Detail() {
    const { id } = useParams();

    const { data, isFetching } = useQuery<AnimeType>({
        queryKey: ['anime'],
        queryFn: () => apiServices.getDetail(id as string),
    });

    return (
        <div css={style_main_container}>
            {isFetching ? (
                <Loading loading={isFetching} />
            ) : (
                <div css={style_container}>
                    <div css={style_image_poster_container}>
                        <img
                            alt="image"
                            src={data?.attributes.posterImage.large}
                            css={style_image_poster}
                            height={600}
                            width={420}
                        />
                    </div>
                    <div css={style_content}>
                        <h1 css={style_title}>
                            {data?.attributes?.titles?.en_jp} (
                            {data?.attributes.titles?.ja_jp})
                        </h1>
                        <div css={style_tag}>
                            <div css={style_popularity}>
                                Ranked #{data?.attributes?.ratingRank}
                            </div>
                            <div css={style_popularity}>
                                Popularity #{data?.attributes?.popularityRank}
                            </div>
                            <div css={style_popularity}>
                                Favorites #
                                {data?.attributes?.favoritesCount?.toLocaleString()}
                            </div>
                        </div>
                        <div css={style_content_rating_cover_image}>
                            <div>
                                <div css={styled_score_title}>SCORE</div>
                                <h1 css={styled_score}>
                                    {data?.attributes.averageRating}
                                </h1>
                                <p css={styled_user}>
                                    {data?.attributes.userCount?.toLocaleString()}{' '}
                                    Users
                                </p>
                            </div>
                            <div css={style_image_cover_container}>
                                <img
                                    alt={'image'}
                                    src={data?.attributes?.coverImage?.large}
                                    css={style_image_cover}
                                />
                            </div>
                        </div>
                        <h3>Synopsis</h3>
                        <p css={styled_synopsis}>
                            {data?.attributes?.synopsis}
                        </p>
                        <h3>Description</h3>
                        <p css={styled_synopsis}>
                            {data?.attributes?.description}
                        </p>
                    </div>
                </div>
            )}
        </div>
    );
}

export default Detail;
