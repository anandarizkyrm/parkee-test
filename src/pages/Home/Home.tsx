/** @jsxImportSource @emotion/react */
import { useQuery } from '@tanstack/react-query';
import { useEffect, useState } from 'react';
import Loading from '../../components/atoms/Loading/Loading';
import Card from '../../components/molecules/Card/Card';
import Pagination from '../../components/molecules/Pagination/Pagination';
import apiServices from '../../services/api.service';
import { AnimeType } from '../../types/animes';
import { style_animes_container } from './Style';

function Home() {
    const [page, setPage] = useState(Number(localStorage.getItem('page')) || 1);
    const [stateData, setStateData] = useState<any>();
    const limitPage = 10;

    const { data, isLoading } = useQuery({
        queryKey: ['animes', page],
        queryFn: () => apiServices.getAnimes((page - 1) * limitPage, limitPage),
    });

    useEffect(() => {
        if (data) {
            setStateData(data);
        }
    }, [data]);

    return (
        <>
            <Loading loading={isLoading} />
            {stateData ? (
                <>
                    <div css={style_animes_container}>
                        {stateData?.data.map((item: AnimeType) => (
                            <Card
                                key={item?.id}
                                averageRating={item?.attributes?.averageRating}
                                backgroundUrl={
                                    item?.attributes?.posterImage?.large
                                }
                                episodeCount={item?.attributes?.episodeCount}
                                popularityRank={
                                    item?.attributes?.popularityRank
                                }
                                showType={item?.attributes?.showType}
                                slug={item?.attributes?.slug}
                                title={item?.attributes?.titles}
                                userCount={item?.attributes?.userCount}
                                id={item?.id}
                            />
                        ))}
                    </div>
                    {!isLoading ? (
                        <Pagination
                            page={page}
                            setPage={setPage}
                            limitPage={limitPage}
                            totalData={data?.meta?.count}
                        />
                    ) : null}
                </>
            ) : null}
        </>
    );
}

export default Home;
