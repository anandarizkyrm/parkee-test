import { css } from '@emotion/react';

export const style_animes_container = css`
    display: grid;
    grid-template-columns: repeat(5, minmax(0, 1fr));
    margin-bottom: 12px;
    gap: 6px;
    padding: 4px;

    @media (max-width: 1024px) {
        grid-template-columns: repeat(3, minmax(0, 1fr));
    }

    @media (max-width: 768px) {
        grid-template-columns: repeat(2, minmax(0, 1fr));
    }

    @media (max-width: 480px) {
        grid-template-columns: repeat(1, minmax(0, 1fr));
    }
`;

export const style_content = css`
    height: 15%;
    background-color: rgba(0, 0, 0, 0.7);
    bottom: 0;
    color: #fff;
    display: block;
    box-sizing: border-box;
    left: 0;
    padding: 4px;
    width: 100%;
    position: absolute;
`;

export const style_title = css`
    font-size: 0.75rem;
`;

export const style_header = css`
    font-size: 2rem;
    border-left: solid 6px #408bea;
    display: flex;
    align-items: center;
    padding-bottom: 4px;
    border-color: rgba(0, 0, 0, 0.5);
    padding-left: 0.5rem;
    margin: 12px 6px 12px 6px;
    cursor: pointer;

    @media (max-width: 768px) {
        font-size: 1.2rem;
    }
`;

export const style_misc = css`
    display: flex;
`;

export const style_type = css`
    display: flex;
`;

export const style_rating = css`
    display: flex;
`;

export const style_people = css`
    display: flex;
`;
