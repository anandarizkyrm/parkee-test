import { createRoot } from 'react-dom/client';
import { QueryClientProvider } from '@tanstack/react-query';
import React from 'react';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import { Global } from '@emotion/react';
import { globalStyles } from './Style';
import { queryClient } from './utils/queryClient';
import Navbar from './components/layout/Navbar/Navbar';

const container = document.getElementById('root') as HTMLDivElement;

const root = createRoot(container);
root.render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <Global styles={globalStyles as any} />
                <div
                    style={{
                        maxWidth: '1380px',
                        position: 'relative',
                        margin: '0 auto',
                    }}
                >
                    <Navbar />
                    <App />
                </div>
            </BrowserRouter>
            <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
    </React.StrictMode>,
);
